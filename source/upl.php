<?php
	if ( 0 < $_FILES['file']['error'] ) {
        echo 'Error: ' . $_FILES['file']['error'] . '<br>';
    } else {
    	$array = explode('.', $_FILES['file']['name']);
    	$extension = end($array);
    	$filenameServer = generateRandomString() . "." . $extension;

        move_uploaded_file($_FILES['file']['tmp_name'], 'uploads/' . $filenameServer);
		echo "uploads/" . $filenameServer;
    }

    function generateRandomString($length = 10) {
	    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;
	}
	
?>