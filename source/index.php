<html>
<head>
	<title>Finnish memes</title>
	<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
  <link rel="stylesheet" href="css/styles.css" />
  
	</head>
	<body>
		<nav class="navbar navbar-expand-lg navbar-light bg-light sticky" style="line-height: 30px;">
				  <a class="navbar-brand" href="#"><small>SAATANAPERKLE</small></a>
				  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
				    <span class="navbar-toggler-icon"></span>
				  </button>
				  <div class="collapse navbar-collapse" id="navbarSupportedContent">
				    <ul class="navbar-nav mr-auto">
				      
				      <li class="nav-item" >
				        <a class="nav-link" href="javascript:void(0)" onclick="p1()"><small>HOT</small> <span class="sr-only">(current)</span></a>
				      </li>
				      <li class="nav-item" style="float: right;">
				        <a class="nav-link" href="javascript:void(0)" onclick="p2()"><small>TRENDING</small></a>
				      </li>
				      <li class="nav-item" style="float: right;">
				        <a class="nav-link" href="javascript:void(0)" onclick="p3()"><small>FRESH</small></a>
				      </li>
				      <li class="nav-item dropdown" style="display: none;">
				        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				          Dropdown
				        </a>
				        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
				          <a class="dropdown-item" href="#">Action</a>
				          <a class="dropdown-item" href="#">Another action</a>
				          <div class="dropdown-divider"></div>
				          <a class="dropdown-item" href="#">Something else here</a>
				        </div>
				      </li>
				      <li class="nav-item" style="display: none;">
				        <a class="nav-link disabled" href="#">Disabled</a>
				      </li>
				    </ul>
				    <form class="form-inline my-2 my-lg-0">
				    	<a class="nav-link" href="#" data-toggle="modal" data-target="#exampleModal"><small>UPLOAD</small></a>
				      <input class="form-control mr-sm-2" id="search-input" type="search" placeholder="Search" aria-label="Search">
				      <button class="btn btn-outline-success my-2 my-sm-0" onclick="pfilter()" type="submit" style="height: 30px;"><small><img src="icns-svg/arrow-right.svg"/></small></button>
				    </form>
				  </div>
				</nav>
		<div class="wrapper" id="bodywrapper">
			<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			  <div class="modal-dialog" role="document">
			    <div class="modal-content">
			      <div class="modal-header">
			        <h5 class="modal-title" id="exampleModalLabel">New post</h5>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
			      </div>
			      <div class="modal-body">
			        <form>
			        	<input type="text" id="post_title" class="form-control" placeholder="Post title" /><br>
			        	<input type="file" name="file" id="fileU" class="form-control" placeholder="Post img" style="height: 44px;"/>
			        </form>
			      </div>
			      <a href="#" style="float: left; padding-left: 20px;">Report problem?</a><br>
			      <div class="modal-footer">
			        
			        <button type="button" class="btn btn-success" id="submitPost" style="float: right;">Upload</button>
			      </div>
			    </div>
			  </div>
			</div>
			<script>
				$("#submitPost").click(function() {
		            var post_title = $("#post_title").val();

		            var file_data = $('#fileU').prop('files')[0];   
		            var form_data = new FormData();                  
		            form_data.append('file', file_data);         

		            $.ajax({
						url:'upl.php',
						method: 'POST',
						data: form_data,
						datatype: 'text', 
						cache: false,
						contentType: false,
						cache: false,
						processData: false,
						beforeSend:function(){
							//$('#msg').html('Loading......');
							console.log("loading...");
						},
						success:function(data){

							$.ajax({
								url:'api.php',
								method: 'POST',
								data: {"call" : "new_post", "param1" : post_title, "param2" : data},
								datatype: 'JSON', 
								success:function(data2){
									console.log("data2: " + data2);
									//$('#msg').html(data);
									location.reload();
								}
							});
							//$('#msg').html(data);
						}
					});
				});
			</script>
			<script>
				function p1 () {
					$("#search-input").empty();
					$("#search-input").val("#hot");
					$("#containerId").empty();
					$.ajax({
		                type: "POST",
		                url: "php_load/get_filtered.php",
		                data: {"filter" : "hot"},
    					dataType:'text', 
		                success: function(response){
		                    //console.log("response: "+response);
		                    $("#containerId").append(response);
		                }
		            });
				}
				function p2 () {
					$("#search-input").empty();
					$("#search-input").val("#trending");
					$.ajax({
		                type: "POST",
		                url: "php_load/get_filtered.php",
		                data: {"filter" : "trending"},
    					dataType:'text', 
		                success: function(response){
		                    //console.log("response: "+response);
		                    $("#containerId").empty();
		                    $("#containerId").append(response);
		                }
		            });
				}
				function p3 () {
					$("#search-input").empty();
					$("#search-input").val("#fresh");
					$.ajax({
		                type: "POST",
		                url: "php_load/get_filtered.php",
		                data: {"filter" : "fresh"},
    					dataType:'text', 
		                success: function(response){
		                    //console.log("response: "+response);
		                    $("#containerId").empty();
		                    $("#containerId").append(response);
		                }
		            });
				}
				function pfilter () {
					$("#search-input").empty();
					var filter = $("#search-input").val().replace('#', '');
					$.ajax({
		                type: "POST",
		                url: "php_load/get_filtered.php",
		                data: {"filter" : filter},
    					dataType:'text', 
		                success: function(response){
		                    //console.log("response: "+response);
		                    $("#containerId").empty();
		                    $("#containerId").append(response);
		                }
		            });
				}
			</script>
				
			<script type="text/javascript">

				$("#search-input").val("#hot");

		        function scrolled(o)
		        {
		            //visible height + pixel scrolled = total height 
		            if(o.offsetHeight + o.scrollTop == o.scrollHeight)
		            {
		                alert("End");
		            }
		        }

		        $("#search-input").keydown(function(e) {
					var oldvalue=$(this).val();
					var field=this;
					setTimeout(function () {
					    if(field.value.indexOf('#') !== 0) {
					        $(field).val(oldvalue);
					    } 
					}, 1);
					});
		    </script>
			<div onscroll="scrolled(this)" class="container" id="containerId">
				<?php
			    $servername = "localhost";
					$username = "root";
					$password = "root";
					$dbname = "fm";

					//$no = $_POST["getresult"];

					$conn = new mysqli($servername, $username, $password, $dbname);
					// Check connection
					if ($conn->connect_error) {
					  die("Connection failed: " . $conn->connect_error);
					} 

					$sql = "SELECT * FROM posts ORDER BY id LIMIT 10";
					$result = $conn->query($sql);

					if ($result->num_rows > 0) {
					  while($row = $result->fetch_assoc()) {
					    ?>
					    <div class="post">
					    <div class="post-title"><h2><?php echo $row["post_title"]; ?></h2></div>
					    <div class="post-img"><img src="<?php echo $row['post_img']; ?>" style="max-width:600px; max-height: 350px;"/></div>
					    <div class="post-actions">
				          <a href="#"><span class="badge badge-default"><img src="icns-svg/arrow-thick-top.svg"/> <small><?php echo $row['post_upvotes']; ?></small></span></a> <a href="#"><span class="badge badge-default"><img src="icns-svg/arrow-thick-bottom.svg"/> <small><?php echo $row['post_downvotes']; ?></small></span></a> <a href="" style="float: right;"><small>Report</small></a> 
				        </div>
					  </div>
					    <?php
					    
					  //echo '<a class="dropdown-item" id="ldapselecthref" onclick="selectLdap('."'".$row["ldap"]."'".')" href="javascript:void(0)">'.$row["ldap"].'</a>';
					  }
					} else {
					  echo "0 results";
					}
					$conn->close();

				?>

				<div class="post" style="display: none;">
					<div class="post-title"><h2>Post title</h2></div>
					<div class="post-img"><img src="https://pics.me.me/kokdokokokokkolcokoonl-koko-kokkokopkoko-kokko-isa-finnish-13995205.png" /></div>
					<div class="post-actions">
						<p>hrllo</p>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>