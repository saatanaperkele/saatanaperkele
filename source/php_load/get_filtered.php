<?php
  $servername = "localhost";
  $username = "root";
  $password = "root";
  $dbname = "fm";

  //$no = $_POST["last_id"];
  $filter = $_POST["filter"];

  $conn = new mysqli($servername, $username, $password, $dbname);
  // Check connection
  if ($conn->connect_error) {
      die("Connection failed: " . $conn->connect_error);
  } 

  $sql = "SELECT * FROM posts WHERE post_cat = '$filter' ORDER BY id LIMIT 30";
  $result = $conn->query($sql);

  if ($result->num_rows > 0) {
      while($row = $result->fetch_assoc()) {
        ?>
        <div class="post">
        <div class="post-title"><h2><?php echo $row["post_title"]; ?></h2></div>
        <div class="post-img"><img src="<?php echo $row['post_img']; ?>" style="max-width:600px; max-height: 350px;"/></div>
        <div class="post-actions">
          <a href="#"><span class="badge badge-default"><img src="icns-svg/arrow-thick-top.svg"/> <small><?php echo $row['post_upvotes']; ?></small></span></a> <a href="#"><span class="badge badge-default"><img src="icns-svg/arrow-thick-bottom.svg"/> <small><?php echo $row['post_downvotes']; ?></small></span></a> <a href="" style="float: right;"><small>Report</small></a> 
        </div>
      </div>
        <?php
        
      //echo '<a class="dropdown-item" id="ldapselecthref" onclick="selectLdap('."'".$row["ldap"]."'".')" href="javascript:void(0)">'.$row["ldap"].'</a>';
      }
  } else {
      ?>
        <div class="post">
        <div class="post-title"><h2>Oh no.... Looks likes theres no posts here!</h2></div>
        <div class="post-img"><img src="https://139448-507445-raikfcquaxqncofqfm.stackpathdns.com/wp-content/uploads/2018/07/no-img-600x400-600x400.jpg" style="max-width:600px; max-height: 350px;"/></div>
        <div class="post-actions">
          
        </div>
      </div>
        <?php
  }
  $conn->close();

?>