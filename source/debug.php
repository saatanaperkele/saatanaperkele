<html>
<head>
		<title>Debug tool</title>
		<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.2/rollups/aes.js"></script>
		<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>

		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	</head>
	<body>
		<h1>Debug tool</h1>
		<br>
		<div id="" class="" style="width: 50%; margin: 0 auto; border: 1px solid #ccc; border-radius: 5px; padding: 15px;">
			<h2>crypt test</h2>
			<form method="post" action="javascript:void(0)" class="form-group">
				<label>encrypt:</label>
				<input type="text" name="plain" id="plain" class="form-control" />
				<label>decrypt:</label>
				<input type="text" name="crypt" id="crypt" class="form-control"/><br>
				<!--<label>readable:</label>
				<input type="text" name="decrypt" id="crypt" class="form-control"/><br>-->
				<input type="submit" name="submit" id="submitEncrypt" value="encrypt" class="btn btn-danger" />
				<input type="submit" name="submit" id="submitDecrypt" value="decrypt" class="btn btn-primary"/>
			</form>
		</div>
		<br><br>
		<div id="" class="" style="width: 50%; margin: 0 auto; border: 1px solid #ccc; border-radius: 5px; padding: 15px;">
			<h2>api test</h2>
			<form method="post" action="javascript:void(0)" class="form-group">
				<label>url:</label>
				<input type="text" name="apiurl" id="apiurl" class="form-control" readonly="true" />
				<label>call:</label>
				<input type="text" name="apicall" id="apicall" class="form-control"/><br>

				<input type="text" placeholder="param" name="param1" id="param1" class="form-control" style="width: 45%; float: left; margin-right: 10%;"/><input type="text" placeholder="value" name="param1value" id="param1value" class="form-control" style="width: 45%;"/><br>
				<!--<label>param2:</label>-->
				<input type="text" placeholder="param" name="param2" id="param2" class="form-control" style="width: 45%; float: left; margin-right: 10%;"/><input type="text" placeholder="value" name="param2value" id="param2value" class="form-control" style="width: 45%;"/><br>

				<input type="text" placeholder="param" name="param3" id="param3" class="form-control" style="width: 45%; float: left; margin-right: 10%;"/><input type="text" placeholder="value" name="param3value" id="param3value" class="form-control" style="width: 45%;"/><br>

				<input type="text" placeholder="param" name="param4" id="param4" class="form-control" style="width: 45%; float: left; margin-right: 10%;"/><input type="text" placeholder="value" name="param4value" id="param4value" class="form-control" style="width: 45%;"/><br>

				<input type="text" placeholder="param" name="param5" id="param5" class="form-control" style="width: 45%; float: left; margin-right: 10%;"/><input type="file" placeholder="value" name="file" id="file" class="form-control" style="width: 45%;"/><br>
				<!--<label>readable:</label>
				<input type="text" name="decrypt" id="crypt" class="form-control"/><br>-->
				<input type="submit" name="submit" id="submitApiCall" value="test" class="btn btn-primary"/>
			</form>
		</div>
		<br><br>
		<div id="" class="" style="width: 50%; margin: 0 auto; border: 1px solid #ccc; border-radius: 5px; padding: 15px; background-color: #eee;">
			<center><small><p id="outputtext">output</p></small></center>
			<textarea class="form-control" id="apiOutput" style="font-size: 14px; color: #333; height: 40%; width: 100%; resize: none; background-color: #eee; border: none; display: none;" readonly="true"></textarea>
		</div>

			<script>
				$("#submitEncrypt").click(function() {
				  //alert(this.id);
				  var plain = $("#plain").val();
				  encryptText(plain);
				});

				$("#submitDecrypt").click(function() {
				  //alert(this.id);
				  var crypt = $("#crypt").val();
				  decryptText(crypt);
				});
				

				$("#submitApiCall").click(function() {
				  //alert(this.id);
				  //var plain = $("#plain").val();
				  //encryptText(plain);
				  var apiCall = $("#apicall").val();
				  var apiParam1 = $("#param1value").val();
				  var apiParam2 = $("#param2value").val();

				  console.log("api call: "+apiCall);

				  	$.ajax({
		                type: "POST",
		                url: "api.php",
		                data: {"call" : apiCall, "param1" : apiParam1, "param2" : apiParam2},
    					dataType:'JSON', 
		                success: function(response){
		                    console.log("result: " + JSON.stringify(response))
		                    //$("#apiOutput").empty();
		                    $("#apiOutput").show();
		                    $("#apiOutput").empty();
		                    $("#apiOutput").text(JSON.stringify(response));
		                }
		            });
				});

				function encryptText (text) {
					var encrypted = CryptoJS.AES.encrypt(text, "Secret Passphrase");
					//var decrypted = CryptoJS.AES.decrypt(encrypted, "Secret Passphrase");
					//4d657373616765

					console.log("ENC: "+encrypted);
					$("#crypt").empty();
					$("#crypt").val(encrypted);
					//console.log("DEC: "+decrypted);
					//console.log("DEC2S: "+decrypted.toString(CryptoJS.enc.Utf8));

					/*$("#demo1").append(encrypted);
					$("#demo2").append(decrypted);
					$("#demo3").append(decrypted.toString(CryptoJS.enc.Utf8));*/
				}

				function decryptText (text) {
					//var encrypted = CryptoJS.AES.encrypt(text, "Secret Passphrase");
					var decrypted = CryptoJS.AES.decrypt(text, "Secret Passphrase");
					//4d657373616765

					//console.log("ENC: "+encrypted);
					console.log("DEC: "+decrypted);
					console.log("DEC2S: "+decrypted.toString(CryptoJS.enc.Utf8));

					$("#plain").empty();
					$("#plain").val(decrypted.toString(CryptoJS.enc.Utf8));
				}
				//U2FsdGVkX18ZUVvShFSES21qHsQEqZXMxQ9zgHy+bu0=

				
			</script>

			<br><br>
			<label>encrypted</label>
			<div id="demo1"></div>
			<br>

			<label>decrypted</label>
			<div id="demo2"></div>

			<br>
			<label>Actual Message</label>
			<div id="demo3"></div>
	</body>
</html>